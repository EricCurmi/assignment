
/* 
 * File:   main.c
 * Author: Eric
 *
 * Created on 30 October 2017, 10:53
 */

#include <stdio.h>
#include <stdlib.h>
#define MAX 9999

//Structures

typedef struct corridors
    {
        int c1, 
            c2,
            time;
        struct corridors *next;
    }cor;
    
typedef struct short_path
    {
        int cham;
        struct adjacency_matrix *arr;
    }path;   
    
typedef struct adjacency_matrix
    {
        struct corridors *start;  
    }mat;

typedef struct heap_chamber
{
    int  chambers,
         time;
}hc;
 
typedef struct heap_array
{
    int thc,      //total number of chambers in heap
        limit,    //size limit of the heap 
        *pos;     // fix// This is needed for decreaseKey()
    struct heap_chamber **arrheap;
}heap;    
    
//Headers
void check(struct corridors *ptr, int x, int y, int z, int *e);

void linked_list(struct corridors *ptr, int a,int b, int c);

cor *add_matrix(int cham, int time);

void create_connection(path *route, int cham1, int cham2, int t);

path *plotter(int n);

void swap(hc **c1, hc **c2);

hc *add_heap_chamber(int n, int t);

heap *create_heap(int lim);

int checkempty(heap *ptr);

void heapify(heap *ptr, int update);

hc *detHeap(heap *ptr);

int checkheap(heap *ptr, int n);

void currentpos(heap *ptr, int n, int time);

void output_shortestpath(int totaltime[], int n, int *e, int k);




// Main

void main() 
{
    int N,M,K,*exit,i, j; 
    
    cor *a;
    
    FILE *inp = fopen("input.txt", "r"); // reading from txt file
    
    if((inp = fopen("input.txt","r"))== NULL) // verifying txt file exists
    {
        printf("NOT FOUND");
    }
    
    fscanf(inp,"%d %d %d",&N,&M,&K); // reading first line of txt file
    exit = malloc(sizeof(int)*K); // setting exit array size according to number of exits
    a = (cor*)malloc (sizeof(cor)*M); // setting the structure array size according to number of corridors
    path *route = plotter(N);//sets the size of the structure according to the number of chambers

    for(i = 0; i < K; i++)
    {
        fscanf(inp,"%d",&exit[i]); //reading second line of txt file
    }
    

    for(i = 0; i < M; i++)
    {
        fscanf(inp,"%d %d %d",&a->c1,&a->c2,&a->time); //reading the rest of the lines left in the txt file
        linked_list(a,a->c1,a->c2,a->time); //inserting the data from the txt to the linked list
        if( i == M-1)
        {
            a = a->next; //to put the pointer back to the start of the list
        }
    }

    //check(a,N,M,K,exit);//checking to see if all the data is stored correctly
    
    while( a != NULL )
    {
        create_connection(route, a->c1, a->c2, a->time);
        a = a->next;
    }
    
    dijkstra_algorithm(route, 0, exit, K);
    
    fclose(inp);//closing txt file
}

//Functions

check(struct corridors *ptr, int x, int y, int z, int *e) //checking if data read from the input file is saved correctly
{
    struct corridors *current = ptr;
    int j;

    printf("%d %d %d\n",x,y,z); 
    
    for(j = 0; j < z; j++)
    {
        printf("%d ",e[j]);
    }
    printf("\n");
    

    while (current != NULL) 
    {
        printf("Chamber %d to %d time: %d mins\n", current->c1,current->c2,current->time);//displaying current corridor in the linked list
        current = current->next;//moving to the next corridor in the linked list
    }
    printf("\n");
}


void linked_list(struct corridors *ptr, int cham1,int cham2, int time) //storing data from input file to linked list
{
    struct corridors *current = ptr;
   
    //moving the pointer
    while (current->next != NULL) 
    {
        current = current->next;
    }
    //adding another corridor from the txt file to the linked list
    current->next = malloc(sizeof(struct corridors)); 
    current->next->c1 = cham1;
    current->next->c2 = cham2;
    current->next->time = time;
    current->next->next = NULL;
}

cor *add_matrix(int cham, int time) //adding more chambers to the adjacency matrix
{
    cor *add = malloc(sizeof(cor));
    add->c1 = cham;
    add->time = time;
}

void create_connection(path *route, int cham1, int cham2, int t)
{
    //creating the corridor connection in the graph(cham1 to cham2)
    cor *add = add_matrix(cham2, t);
    add->next = route->arr[cham1].start;
    route->arr[cham1].start = add;
    //creating the corridor connection in the graph(cham2 to cham1) since the corridor can be traversed both ways
    add = add_matrix(cham1, t);
    add->next = route->arr[cham2].start;
    route->arr[cham2].start = add;
}

path *plotter(int n) //sets of the size of the graph according to number of chambers
{
    path *route = malloc(sizeof(path));
    route->cham = n;
    route->arr = malloc(sizeof(mat)*n);
    for (int i = 0; i < n; i++)//setting the adjacency matrix to start from the beginning
    {
        route->arr[i].start = NULL;
    }
    return route;
}

void swap(hc **c1, hc **c2) //swapping chambers
{
    hc *sw = *c1;
    *c1 = *c2;
    *c2 = sw;
}

hc *add_heap_chamber(int n, int t) //adding a chamber to the heap
{
    hc *ptr = malloc(sizeof(hc));
    ptr->chambers = n;
    ptr->time = t;
}

heap *create_heap(int lim)//creating the min heap
{
    heap *create = malloc(sizeof(heap));
    create->pos = malloc(sizeof(int)*lim);
    create->limit = lim;
    create->arrheap =  malloc(sizeof(hc*)*lim);
}

int checkempty(heap *ptr) // to see if the heap is empty or not
{
    return ptr->thc == 0;
}

void heapify(heap *ptr, int update) // rearranges heap to maintain the min heap property
{
    int temp, opt1, opt;
    temp = update;
    opt = 2 * update + 2;
    opt1 = 2 * update + 1;
    //choosing which direction to go to, depending on smallest option
    
    if ((opt < ptr->thc) && (ptr->arrheap[opt]->time < ptr->arrheap[temp]->time))
    {
        temp = opt;
    }
    
    if ((opt1 < ptr->thc) && (ptr->arrheap[opt1]->time < ptr->arrheap[temp]->time))
    {
        temp = opt1;
    }
 
    if (temp != update) // used to select and swap the chambers in order to satisfy the min heap property
    {
        hc *updatedCham = ptr->arrheap[update];
        hc *tempCham = ptr->arrheap[temp];
       
        ptr->pos[updatedCham->chambers] = temp;
        ptr->pos[tempCham->chambers] = update;
        swap(&ptr->arrheap[temp], &ptr->arrheap[update]);

        heapify(ptr, temp); //recursion used to keep repeating function till the min heap property is met
    }
}

hc *detHeap(heap *ptr)//determining the shortest route available in the heap
{
    if (checkempty(ptr))
    {
        return NULL;
    }
    hc *startingCham = ptr->arrheap[0]; //putting current starting chamber in array
    hc *currentCham = ptr->arrheap[ptr->thc - 1]; //replacing starting chamber with current chamber
    ptr->arrheap[0] = currentCham ;
    // updating the location of the current chamber
    ptr->pos[currentCham ->chambers] = 0;
    ptr->pos[startingCham->chambers] = ptr->thc-1;
    //the size of the heap is reduced and the starting chamber is heapified
    --ptr->thc;
    heapify(ptr, 0);
    return startingCham;
}

int checkheap(heap *ptr, int n) // checking if the chamber is in the heap
{
    if (ptr->pos[n] < ptr->thc)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

void currentpos(heap *ptr, int n, int time) //updates the position in heap
{
    int i = ptr->pos[n]; //current position
 
    ptr->arrheap[i]->time = time; //updates total time
 
    //swapping chambers before being put in the heap to find the total time
    while (i && ptr->arrheap[i]->time < ptr->arrheap[(i - 1) / 2]->time)
    {
        ptr->pos[ptr->arrheap[(i-1)/2]->chambers] = i;
        ptr->pos[ptr->arrheap[i]->chambers] = (i-1)/2;
        swap(&ptr->arrheap[i],  &ptr->arrheap[(i - 1) / 2]);
        i = (i - 1) / 2;
    }
}

void output_shortestpath(int totaltime[], int n, int *e, int k)//finds shortest route to all existing chambers
{
    int i, j,temp = MAX;
    FILE *Output;
    if((Output = fopen("Output.txt","w"))== NULL)
    {
        printf("NOT FOUND");
    }
    
    for (i = 0; i < n; ++i)
    {
        for( j = 0; j < k; j++)
        {
            if (e[j] == i) //limits results to only exit chambers
            {
                if(totaltime[i] < temp) //chooses the exit chamber with the shortest time
                {
                    temp = totaltime[i]; 
                }
            } 
        }
    }
    fprintf(Output,"Shortest path to exit takes %d minutes\n",temp);
    fclose(Output);
   
    printf("Shortest exit is %d minutes away from chamber 0.\n",temp);
}

void dijkstra_algorithm(path *route, int start, int *e, int k)
{
    int c, graphcham, temp, list;
    graphcham = route->cham; 
    int totaltime[graphcham];
    
    heap *ptr = create_heap(graphcham);
 
    for (c = 0; c < graphcham; c++) //starting min heap with all the chambers and corridors
    {
        totaltime[c] = MAX;
        ptr->pos[c] = c;
        ptr->arrheap[c] = add_heap_chamber(c, totaltime[c]);
        
    }
 
    // sets time of starting chamber to 0 and sets it as the root of the min heap
    ptr->arrheap[start] = add_heap_chamber(start, totaltime[start]);
    ptr->pos[start] = start;
    totaltime[start] = 0;
    currentpos(ptr, start, totaltime[start]);

    //Sets the size of the min heap according to the amount of chambers
    ptr->thc = graphcham;
 

    while (!checkempty(ptr))
    {
        hc *shortest_route = detHeap(ptr); // chooses the closest chamber

        temp = shortest_route->chambers; 

        cor *next_cham = route->arr[temp].start;
        while (next_cham != NULL) //goes through the adj list and updates the times
        {
            list = next_cham->c1;

            //since this process goes through all options, the answer might be found
            //early, this if condition compares older options for the best result if
            //the result is not found after going through all the possible choices
            if (checkheap(ptr, list) && totaltime[temp] != MAX && next_cham->time + totaltime[temp] < totaltime[list])
            {
                totaltime[list] = totaltime[temp] + next_cham->time;
                currentpos(ptr, list, totaltime[list]);
            }
            next_cham = next_cham->next;
        }
    }
    output_shortestpath(totaltime, graphcham, e, k);
}